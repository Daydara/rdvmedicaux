<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
  {

/**
  * @IsGranted("ROLE_ADMIN")
  * @Route("/adminDashboard", name="admin_dashboard")
*/

public function adminDashboard()
  {
    return new Response ("test");
  }

}