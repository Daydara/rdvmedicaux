<?php

namespace App\Controller;

use App\Entity\Medecin;
use App\Entity\Avis;
use App\Form\AvisType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class MedecinController extends AbstractController
{
    /**
     * @Route("/medecin", name="medecin_index")
     */
    public function index()
    {
      $repository = $this->getDoctrine()->getRepository(Medecin::class);

    $medecins = $repository->findAll();

    return $this->render('medecin/index.html.twig', array('medecins'=> $medecins));

    }

    /**
    * @Route("/medecin/{id<\d+>}", name="medecin_show")
    */
    public function show($id){

      $entityManager = $this->getDoctrine()->getManager();
      // On récupère le repository
       $repository = $entityManager->getRepository(Medecin::class);

       $medecin = $repository->find($id);

      return $this->render('medecin/show.html.twig',
                    array("medecin" => $medecin));
    }



    /**
    * @Route("/medecin/menu", name="medecin_menu")
    */
    public function menu(){

      $liste = array(
             array('title' => 'Accueil', 'path' => "medecin_index"),
             array('title' => 'Les médecins', 'path' => "medecin_index"),
             array('title' => 'Avis', 'path' => "avis")
           );

           return $this->render('medecin/menu.html.twig', array('liste' => $liste));
    }


    /**
    * @Route("/medecin/create", name="medecin_create")
    */
    public function create(){
      $entityManager = $this->getDoctrine()->getManager();

      $medecin = new Medecin();
      $medecin->setNom("Schtroumpfs");
      $medecin->setVille("foret");
      $medecin->setDescription("bleu");

      $entityManager->persist($medecin);
      $entityManager->flush();
      return new Response("Le medecin a été crée, il a l'id :" . $medecin->getId());
    }


    /**
    * @Route("/avis", name="avis")
    */
    public function avis(Request $request){

      $avis = new Avis();

      $form =  $this->createForm(AvisType::class, $avis);


       $form->handleRequest($request);
       if ($form->isSubmitted() && $form->isValid()) {
         // on récupère les valeurs
         $avis = $form->getData();

         $avis->setDateCreation(new \Datetime());

         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($avis);
         $entityManager->flush();

         return $this->redirectToRoute('medecin_index');
     }


       return $this->render('avis.html.twig', array('form' => $form->createView()));

    }

}
